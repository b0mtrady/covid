import os
import json 
import pandas as pd 

dirs = ["biorxiv_medrxiv"]
docs = []
for d in dirs: 
    for file in os.listdir(f"corona_data/{d}/{d}"):
        file_path = f"corona_data/{d}/{d}/{file}"
        j = json.load(open(file_path, "rb"))

        title = j['metadata']['title']
        try:
            abstract = j['abstract'][0]
        except:
            abstract = ""

        full_text = ""

        for text in j['body_text']:
            full_text += text['text']+'\n\n'
      
        docs.append([title, abstract, full_text]) 

df = pd.DataFrame(docs, columns=['title','abstract','full_text'])
print(df.head())


