[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ggivingg%2Fcovid/master)

In an attempt to contribute to Coronavirus research we are seeking participants for online dialogues and hackathons.

Merging systems thinking with data investigations we welcome anyone interested in rolling up their sleeves and pitching in ideas. We hope we can alleviate anxiety in these difficult times by thinking together about the challenge of our moment.

Contributing to:

* Cord-19 [Research Challenge](https://www.kaggle.com/allen-institute-for-ai/CORD-19-research-challenge/tasks) - comb through the research to answer important questions about Coronavirue ASAP.

* Covid19 Global [Forecasting Challenge](https://www.kaggle.com/c/covid19-global-forecasting-week-1) - identify factors that impact the transmission of Covid-19. 

Let us not forget that this is a highly [contentious subject](https://www.reddit.com/r/datascience/comments/fm17ja/to_all_data_scientists_out_there_crowdsourcing/). 

We should be modest in our claims and mindful that others might ingest our results as fact though we are not experts. 

---
---

## CORD-19 Research Challnege 

### Goal: 

* 1 data set of all the available research on Coronavirus available
* 10 key questions 
* 10 ways we can help the world respond to Coronavirus. 

### How: 
44,000 scholarly articles about Coronavirus released as a dataset. 

With this dataset as well as the data learning in the Forcasting challenge can we use machine learning to identify factors that answer key scientific community questions? 

### Ten Key Questions
* What is know about transmission, incubation, and environmental stability?
* What do we know about COVID-19 risk factors?
* What do we know about virus genetics, origin and evolution?
* What do we know about vaccines and therapeutics?
* What do we know about the geography of virility?
* What do we know about diagnostics and surveillance?
* What do we know about non-pharmaceutical interventions?
* What has been published about medical care?
* What has been published about ethical and social science considerations?
* What has been published about information sharing and inter-sectoral collaboration? 

---
---
## Covid19 Global Forecasting Challenge

### Goal:  

Identify factors that appear to impact the transmission rate of COVID-19.

### How: 

Forecasting confirmed cases and fatalities by finding variables that look like theyimpact the transmission rate. 

In combination with the COVID-19 Open Research Dataset (CORD-19) participants are encourged to pull in, curate and share data sources that might be helpful to identifying the factors that impact transmission rate. 

[Example potential key question](https://www.kaggle.com/c/covid19-global-forecasting-week-1/overview/open-scientific-questions): 

* What do we know about non-pharmaceutical interventions? 
E.g. Dataset on policy actions taken by region (school closures, cancelling large gatherings, self isolation, etc.). 
Data that might help estimate likely compliance with policy actions 
(e.g. concentration of small businesses) and degree of fear (potentially picked up from Google Trends or social media data).
* What is known about transmission, incubation, and environmental stability? 
E.g. Data on temperature, humidity and air pollution by region.
* What do we know about COVID-19 risk factors? 
E.g. Percentage of the population that smokes in a region or country.
* What has been published about medical care? 
E.g. Data on number of doctors, nurses, hospitals by region or country.
nb
