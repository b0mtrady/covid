## Reference 

Lex Flagel, Yaniv Brandvain, Daniel R Schrider, The Unreasonable Effectiveness of Convolutional Neural Networks in Population Genetic Inference, Molecular Biology and Evolution, Volume 36, Issue 2, February 2019, Pages 220–238, [link] (https://doi.org/10.1093/molbev/msy224)

Seokjun Seo, Minsik Oh, Youngjune Park, Sun Kim, DeepFam: deep learning based alignment-free method for protein family modeling and prediction, Bioinformatics, Volume 34, Issue 13, 01 July 2018, Pages i254–i262, [link](https://doi.org/10.1093/bioinformatics/bty275)

Yamada, K.D. Derivative-free neural network for optimizing the scoring functions associated with dynamic programming of pairwise-profile alignment. Algorithms Mol Biol 13, 5 (2018). https://doi.org/10.1186/s13015-018-0123-6

Li, H., Sun, F. Comparative studies of alignment, alignment-free and SVM based approaches for predicting the hosts of viruses based on viral sequences. Sci Rep 8, 10032 (2018). [link](https://doi.org/10.1038/s41598-018-28308-x)

Catanese, H.N., Brayton, K.A. & Gebremedhin, A.H. A nearest-neighbors network model for sequence data reveals new insight into genotype distribution of a pathogen. BMC Bioinformatics 19, 475 (2018). [link](https://doi.org/10.1186/s12859-018-2453-2)

Torng, W., Altman, R.B. 3D deep convolutional neural networks for amino acid environment similarity analysis. BMC Bioinformatics 18, 302 (2017). [link](https://doi.org/10.1186/s12859-017-1702-0)

Muthukrishnan S.M., Ṣahinalp S.C. (2002) Simple and Practical Sequence Nearest Neighbors with Block Operations. In: Apostolico A., Takeda M. (eds) Combinatorial Pattern Matching. CPM 2002. Lecture Notes in Computer Science, vol 2373. Springer, Berlin, Heidelberg [link](https://link.springer.com/chapter/10.1007%2F3-540-45452-7_22#citeas)
